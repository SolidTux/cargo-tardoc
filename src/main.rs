extern crate rustc_serialize;
extern crate toml;
extern crate regex;

use rustc_serialize::json::Json;
use regex::Regex;
use std::fs::{read_dir, create_dir, metadata, copy, File, remove_dir_all,
    rename};
use std::io::{Read, Write};
use std::path::{Path, PathBuf};
use std::process::Command;

static HTML_PRE: &'static str = r#"<html>
<head><meta http-equiv="refresh" content="0; url="#;
static HTML_SUF: &'static str = r#""/></head>
<body></body>
</html>"#;

fn get_config() -> toml::Value {
    let loc;
    let out = Command::new("cargo").arg("locate-project").output().unwrap();
    if let Ok(x) = Json::from_str(&String::from_utf8_lossy(&out.stdout)) {
        loc = x["root"].as_string().unwrap().to_string();
    } else {
        panic!("Failed to get location of Config.toml.");
    }
    let mut conf_res = File::open(loc).expect("Could not open Config.toml");
    let mut conf_con = String::new();
    conf_res.read_to_string(&mut conf_con)
        .expect("Could not read Config.toml");
    let conf_val = toml::Parser::new(&conf_con).parse()
        .expect("Could not parse Config.toml");
    if let &toml::Value::Table(ref y) = &conf_val["package"] {
        if let Some(&toml::Value::Table(ref z)) = y.get("metadata") {
            z.get("tardoc").expect("tardoc key not found").clone()
        } else {
            panic!("package.metadata key not found");
        }
    } else {
        panic!("package key not found");
    }
}

fn copy_dir(src: &Path, dst: &Path) {
    let content = read_dir(src).expect("Error while reading directory");
    if let Ok(dst_meta) = metadata(dst) {
        if !dst_meta.is_dir() {
            panic!("Trying to copy file to dir");
        }
    } else {
        create_dir(dst).expect("Error while creating directory");
    }
    for f in content {
        let file = f.expect("Error while reading file");
        let mut dst_ext = dst.to_path_buf();
        dst_ext.push(file.file_name());
        if file.metadata().expect("Metadata error").is_dir() {
            copy_dir(file.path().as_path(),dst_ext.as_path());
        } else {
            copy(file.path().as_path(),dst_ext.as_path())
                .expect("Error while copying file");
        }
    }
}

fn gen_doc() {
    println!("Generating docs for main module");
    Command::new("cargo").arg("doc").arg("--no-deps").status().unwrap();
    if let toml::Value::Table(conf) = get_config() {
        if let Some(&toml::Value::Array(ref x)) = conf.get("docs") {
            for i in x {
                if let &toml::Value::String(ref s) = i {
                    println!("Generating docs for {}",s);
                    Command::new("cargo").arg("doc").arg("-p")
                        .arg(s).arg("--no-deps").status().unwrap();
                    //TODO handle exit status
                }
            }
            if let Some(&toml::Value::String(ref s)) = conf.get("red") {
                println!("Generating redirect to {}",s);
                let mut html_file = File::create("target/doc/index.html")
                    .expect("Error while opening html file");
                html_file.write_all(HTML_PRE.as_bytes()).unwrap();
                html_file.write_all(s.as_bytes()).unwrap();
                html_file.write_all(HTML_SUF.as_bytes()).unwrap();
            }
        }
    }
}

fn move_pkg() {
    let content = read_dir("target/package")
        .expect("Error while reading directory");
    for f in content {
        let file = f.expect("Error while reading file").path();
        if !file.metadata().expect("Metadata error").is_dir() {
            let mut dst_file = file.clone();
            let file_name = file.file_name().unwrap().to_str().unwrap();
            let re = Regex::new(r"^(.*)\.([^.]?)").unwrap();
            dst_file.set_file_name(re.replace(file_name,"$1-doc.$2"));
            println!("{:?} ⇒ {:?}",file.as_path(),dst_file.as_path());
            rename(file.as_path(),dst_file.as_path())
                .expect("Error while copying file");
        }
    }
}

fn tar_pkg() {
    let content = read_dir("target/package")
        .expect("Error while reading directory");
    for f in content {
        let file = f.expect("Error while reading file").path();
        if !file.metadata().expect("Metadata error").is_dir() {
            let mut dst_file = file.clone();
            let file_name = file.file_name().unwrap().to_str().unwrap();
            let re = Regex::new(r"^(.*)\.([^.]?)").unwrap();
            dst_file.set_file_name(re.replace(file_name,"$1.tar.gz"));
            println!("{:?} ⇒ {:?}",file.as_path(),dst_file.as_path());
            rename(file.as_path(),dst_file.as_path())
                .expect("Error while copying file");
        }
    }
}

fn output_pkg(tar: bool, output: &Path) {
    let content = read_dir("target/package")
        .expect("Error while reading directory");
    if let Ok(output_meta) = metadata(output) {
        if !output_meta.is_dir() {
            panic!("Trying to copy file to dir");
        }
    } else {
        create_dir(output).expect("Error while creating directory");
    }
    for f in content {
        let file = f.expect("Error while reading file").path();
        if !file.metadata().expect("Metadata error").is_dir() {
            let mut dst_file = PathBuf::new();
            dst_file.push(output);
            dst_file.push(file.file_name().unwrap().to_str().unwrap());
            if tar {
                dst_file.set_extension("tar.gz");
            }
            println!("{:?} ⇒ {:?}",file.as_path(),dst_file.as_path());
            rename(file.as_path(),dst_file.as_path())
                .expect("Error while copying file");
        }
    }
}


fn main() {
    //TODO cd to package root directory
    let mut verify = true;
    let mut tar = false;
    let mut output = String::from("");
    if let toml::Value::Table(conf) = get_config() {
        if let Some(&toml::Value::Boolean(ref x)) = conf.get("verify") {
            verify = *x;
        }
        if let Some(&toml::Value::Boolean(ref x)) = conf.get("tar") {
            tar = *x;
        }
        if let Some(&toml::Value::String(ref x)) = conf.get("output") {
            output = x.clone();
        }
    }
    gen_doc();
    copy_dir(Path::new("target/doc"),Path::new("doc"));
    println!("Creating package");
    if verify {
        Command::new("cargo").arg("package").status().unwrap();
    } else {
        Command::new("cargo").arg("package").arg("--no-verify").status()
            .unwrap();
    }
    println!("Rename doc package");
    move_pkg();
    println!("Creating package");
    remove_dir_all("doc").expect("Error while removing folder");
    if verify {
        Command::new("cargo").arg("package").status().unwrap();
    } else {
        Command::new("cargo").arg("package").arg("--no-verify").status()
            .unwrap();
    }
    if output != String::from("") {
        println!("Copying to output directory");
        output_pkg(tar, Path::new(&output));
    } else {
        if tar {
            println!("Rename package to tar");
            tar_pkg();
        }
    }
}
